<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::name("api.")->namespace('API')->group(function () {
    Route::prefix('admin')->name("admin.")->namespace('Admin')->group(function () {
        Route::post('/login', 'AuthController@login')->name('login');
        
        Route::group(['middleware' => ['auth:admin_api']], function () {
            Route::resource('centers', 'CenterController');
        });
    });

    Route::prefix('student')->name("student.")->namespace('Student')->group(function () {
        Route::post('/login', 'AuthController@login')->name('login');
        Route::post('/register', 'AuthController@register')->name('register');
        
                Route::get('/centers', 'CenterController@index');//->name('student.all');
                Route::get('/centers/{center}', 'CenterController@show');//->name('student.all');
                Route::get('/courses/{course}', 'CourseController@show');//->name('student.all');
      
                Route::get('/courses', 'CourseController@index')->name('courses');
        Route::group(['middleware' => ['auth:api']], function () {
            Route::get('/profile', 'AuthController@profile');//->name('student.all');
           
            Route::post('/courses/{course}/subscribtion', 'CourseController@toggleSubscribe');
        });
    });
    

    Route::prefix('center')->name("center.")->namespace('Center')->group(function () {
        Route::post('/login', 'AuthController@login')->name('login');
        
        Route::group(['middleware' => ['auth:api']], function () {
            Route::get('/', 'CenterController@index');
            Route::put('/', 'CenterController@update');
            
            Route::get('/courses', 'CourseController@index');
            Route::post('/courses', 'CourseController@store');
            Route::get('/courses/{course}', 'CourseController@show');
            Route::put('/courses/{course}', 'CourseController@update');
        });
    });

    // Route::resource('centers', CenterController::class);
    // Route::resource('courses', Center\CourseController::class);
});


// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

            //Admin Routes
            // Route::get('/centers','CenterController@index')->name('centers.all');
            // Route::post('/centers','CenterController@store')->name('center.save');
            // Route::get('/centers/{center}','CenterController@show')->name('center.show');
            // Route::put('/centers/{center}','CenterController@update')->name('center.update');
            // Route::delete('/centers/{center}','CenterController@destroy')->name('center.delete');
