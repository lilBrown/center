<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Center extends Model
{
  //  protected $fillable = ['name', 'slugan', 'description', 'phone_1'];
    use HasFactory;

        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','slugan','description','address','lat','lon','phone_1','phon_2','email','facebook'
    ];

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }

    public function courses()
    {
        return $this->hasMany('App\Models\Course');
    }
}
