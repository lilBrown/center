<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;
    
    protected $fillable = ['name','description', 'tutor', 'limit', 'start_date',
                            'end_date', 'price', 'days','is_full', 'status', 'center_id' ];

    public function center()
    {
        return $this->belongsTo('App\Models\Center');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'course_user', 'course_id', 'user_id');
    }
    function registerSeat(){

        if($this->users->count() == $this->limit)
        {
            $this->is_full = true;
            $this->svae();
        }
        return $this;
    }
    function emptySeat(){
        $this->is_full = false;
        $this->save();
        return $this;
    }


    
    function  updateState(){

        if($this->is_full){
            $this->status = 'started';
        }

    }
}
