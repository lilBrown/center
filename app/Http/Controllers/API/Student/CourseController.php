<?php

namespace App\Http\Controllers\API\Student;

use App\Models\Course;
use App\Models\Center;
use Illuminate\Http\Request;
use App\Http\Controllers\API\ApiController;

class CourseController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::with('center')->get();
        return $this->sendResponse('All Courses has been loaded', $courses);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function myCourses()
    {
        $courses = request()->user()->courses;

        return $this->sendResponse('All my Courses has been loaded', $courses);
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function toggleSubscribe(Request $request, Course $course)
    {
        $user = request()->user();

        if ($user->courses->contains($course)) {
            $course->emptySeat()->updateState();
            $msg = "'You have succefully unRegistered'";
            $user->courses()->detach($course);
        } else {
            if (!$course->is_full) {
                $course->registerSeat()->updateState();
                $msg = "'You have succefully registered'";
                $user->courses()->attach($course);
            } else {
                return $this->sendResponse('The course is already full', $user->courses, 401);
            }
        }
        return $this->sendResponse($msg, $course);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        return $this->sendResponse('course is loaded ', $course);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        $course->delete();
        return $this->sendResponse('ok', []);
    }
}
