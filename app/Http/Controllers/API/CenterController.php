<?php
namespace App\Http\Controllers\API;

use App\Models\Center;
use Illuminate\Http\Request;
class CenterController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $centers = Center::all();
        return $this->sendResponse('ok',$centers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'slugan' => 'required',
            'description' => 'required',
            'phone_1' => 'required',
        ]);

        $center = Center::create( $request->all() );

        return $this->sendResponse('ok',$center);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function show(Center $center)
    {
        return $this->sendResponse('ok',$center);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function edit(Center $center)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Center $center)
    {


        $request->validate([
            'name' => 'required',
            'slugan' => 'required',
            'description' => 'required',
            'phone_1' => 'required',
        ]);


        $center->update($request->all());
        return $this->sendResponse('ok',$center);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function destroy(Center $center)
    {
        $center->delete();
        return $this->sendResponse('ok',[]);
    }
}
