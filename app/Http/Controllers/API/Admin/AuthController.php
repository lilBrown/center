<?php

namespace App\Http\Controllers\API\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\API\ApiController;
use Illuminate\Support\Facades\Validator;
use App\Models\Center;

use Auth;

class AuthController extends ApiController
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'=>'required|email',
            'password' =>'required|string',
        ]);

        if ($validator->fails()) {
            return $this->sendError('not authorized',$validator->errors(),401);
        }else{
            
            if(!Auth::guard('admin')->attempt($request->only('email','password'))){
                return $this->sendError('not authorized','Invalid login Data',401);
            }
            $token = Auth::guard('admin')->user()->createToken('AuthToken')->accessToken;
            return $this->sendResponse("Login Succefull",['isAdmin'=>true,'user'=>auth()->guard('admin')->user(),'accessToken'=>$token]);   
            // return $this->sendResponse("Login Succefull",$request->all);   
        }
    }
}
