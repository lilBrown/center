<?php
namespace App\Http\Controllers\API\Admin;

use Illuminate\Support\Facades\Validator;
use App\Models\Center;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\API\ApiController;

class CenterController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $centers = Center::all();
        return $this->sendResponse('hi there',$centers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required|string',
            'slugan'=>'required|string',
            'description'=>'required|string',
            'address'=>'required|string',
            'lat'=>'numeric',
            'lon'=>'numeric',
            'phone_1'=>'required|string|unique:centers,phone_1',
            'phon_2'=>'string',
            'email'=>'required|email|unique:centers,email',
            'facebook'=>'string',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Data incomplete',$validator->errors(),400);
        }else{
            
            $center = Center::create($request->only(['name','slugan','description','address','lat','lon','phone_1','phon_2','email','facebook']));
            
            request()->merge(['phone'=>$request->phone_1,'password'=>bcrypt('password')]);
            $user = User::create(request()->only(['name','phone','email','password']));

            $center->users()->save($user);

            return $this->sendResponse('Cener has been created and user Attached',$center);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function show(Center $center)
    {
        $center->load('courses');
        return $this->sendResponse('Cener has been loaded',$center);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function edit(Center $center)
    {
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Center $center)
    {
        $validator = Validator::make($request->all(), [
            'name'=>'required|string',
            'slugan'=>'required|string',
            'description'=>'required|string',
            'address'=>'required|string',
            'lat'=>'numeric',
            'lon'=>'numeric',
            'phone_1'=>'required|string',
            'phon_2'=>'string',
            'email'=>'required|email|unique:centers,email,'.$center->id,
            'facebook'=>'string',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Data incomplete',$validator->errors(),400);
        }else{
            

            $center->update($request->only(['name','slugan','description','address','lat','lon','phone_1','phon_2','email','facebook']));
            return $this->sendResponse('Cener has been udpated',$center);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function destroy(Center $center)
    {
        $center->delete();

        return $this->sendResponse('Cener has been deleted',['redirect'=>true]);
    }
}
