<?php

namespace App\Http\Controllers\API\Center;

use Illuminate\Http\Request;
use App\Http\Controllers\API\ApiController;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

use Auth;

class AuthController extends ApiController
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'=>'required|email',
            'password' =>'required|string',
        ]);

        if ($validator->fails()) {
            return $this->sendError('not authorized',$validator->errors(),401);
        }else{
            
            if(!Auth::guard('web')->attempt($request->only('email','password'))){
                return $this->sendError('not authorized','Invalid login Data',401);
            }
            $token = Auth::guard('web')->user()->createToken('AuthToken')->accessToken;
            return $this->sendResponse("Login Succefull",['isAdmin'=>false,'user'=>auth()->guard('web')->user(),'accessToken'=>$token]);   
            // return $this->sendResponse("Login Succefull",$request->all);   
        }
    }

    // public function register(Request $request)
    // {
        
    //     $validator = Validator::make($request->all(), [
    //         'name'=>'required|string',
    //         'email'=>'required|email|unique:users,email',
    //         'password' =>'required|string',
    //         'phone'=>'required|string',
    //     ]);

    //     if ($validator->fails()) {
    //         return $this->sendError('not authorized',$validator->errors(),401);
    //     }else{
            
    //         $user = User::create(request()->only(['name','phone','email','password']));

    //         // if(!Auth::guard('user')->attempt($request->only('email','password'))){
    //         //     return $this->sendError('not authorized','Invalid login Data',401);
    //         // }
    //         // $token = Auth::guard('user')->user()->createToken('AuthToken')->accessToken;
    //         return $this->sendResponse("You have Succefully Registered Please login",['task'=>'is done']);   
    //         // return $this->sendResponse("Login Succefull",$request->all);   
    //     }
    //}
}
