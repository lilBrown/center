<?php
namespace App\Http\Controllers\API\Center;

use App\Models\Center;
use Illuminate\Http\Request;
use App\Http\Controllers\API\ApiController;
use Illuminate\Support\Facades\Validator;

class CenterController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $center = request()->user()->center;
        $center->load('courses');
        return $this->sendResponse('Center Profile Loaded Succefully',$center);
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function show(Center $center)
    {
        return $this->sendResponse('ok',$courses);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function edit(Center $center)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $center = request()->user()->center;
        $validator = Validator::make($request->all(), [
            'name'=>'required|string',
            'slugan'=>'required|string',
            'description'=>'required|string',
            'address'=>'required|string',
            'lat'=>'numeric',
            'lon'=>'numeric',
            'phone_1'=>'required|string',
            'phon_2'=>'string',
            'email'=>'required|email|unique:centers,email,'.$center->id,
            'facebook'=>'string',
        ]);

        if ($validator->fails()) {
            return $this->sendError('Data incomplete',$validator->errors(),400);
        }else{
            

            $center->update($request->only(['name','slugan','description','address','lat','lon','phone_1','phon_2','email','facebook']));
            return $this->sendResponse('Cener has been udpated',$center);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Center  $center
     * @return \Illuminate\Http\Response
     */
    public function destroy(Center $center)
    {
        //
    }
}
