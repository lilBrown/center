<?php
namespace App\Http\Controllers\API\Center;

use App\Models\Course;
use Illuminate\Http\Request;
use App\Http\Controllers\API\ApiController;
use Illuminate\Support\Facades\Auth;

class CourseController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = request()->user()->center->courses;
        return $this->sendResponse('All courses have been loaded', $courses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  request()->merge(['center_id' => Auth::user()->center->id]);

        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'tutor' => 'required',
            'days' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'price' => 'required',
        ]);
       
        $course = new Course($request->all());
        request()->user()->center->courses()->save($course);
        return $this->sendResponse('course created succefully', $course);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        return $this->sendResponse('course loaded succefully', $course);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        if ($request->user()->center->courses->contains($course)) {
            $request->validate([
            'name' => 'required',
            'description' => 'required',
            'tutor' => 'required',
            'days' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'price' => 'required',
        ]);

            $course->update($request->all());
            return $this->sendResponse('course updated succefully', $course);
        } else {
            return $this->sendError('not Alowed', ['error_message'=>'you don\'t have premissions to edit this course']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        $course->delete();
        return $this->sendResponse('course deleted succefully', $course);
    }
}
