<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('slugan')->nullable();
            $table->string('description')->nullable();
            $table->string('address')->nullable();
            $table->double('lat')->nullable();
            $table->double('lon')->nullable();
            $table->string('phone_1')->nullable();
            $table->string('phon_2')->nullable();
            $table->string('email')->nullable();
            $table->string('facebook')->nullable();
          
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('centers');
    }
}
